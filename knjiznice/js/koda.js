
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
  
  switch(stPacienta) {
    case 0:
      var ime = "Clark";
      var priimek = "Kent";
      var datumRojstva = "1938-04-18";
      break;
    case 1:
      var ime = "Gandalf";
      var priimek = "The Gray";
      var datumRojstva = "1954-07-29";
      break;
    case 2:
      var ime = "Newt";
      var priimek = "Scamander";
      var datumRojstva = "1879-02-24";
      break;
    default:
      var ime = $("#kreirajIme").val();
      var priimek = $("#kreirajPriimek").val();
      var datumRojstva = $("#kreirajDatumRojstva").val();
      break;
  }

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 || priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " + "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	}
	else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
              switch(stPacienta) {
                case 0:
                  document.getElementById("pac0").value = ehrId;
                  break;
                case 1:
                  document.getElementById("pac1").value = ehrId;
                  break;
                case 2:
                  document.getElementById("pac2").value = ehrId;
                  break;
                default:
                  var option = document.createElement("option");
                  option.text = ime + " " + priimek;
                  option.value = ehrId;
                  var x = document.getElementById("preberiPredlogoBolnika");
                  x.add(option, x[1]);
                  var option1 = document.createElement("option");
                  option1.text = ime + " " + priimek;
                  option1.value = ehrId;
                  var y = document.getElementById("preberiObstojeciEHR");
                  y.add(option1, y[1]);
                  var option2 = document.createElement("option");
                  option2.text = ime + " " + priimek;
                  option2.value = ehrId;
                  var z = document.getElementById("preberiObstojeciVitalniZnak");
                  z.add(option2, z[1]);
              }
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
  }


  return ehrId;
}

function izpisiPodatke(ehrId) {
    $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (data) {
            var party = data.party;
            $("#kreirajIme").val(party.firstNames);
            $("#kreirajPriimek").val(party.lastNames);
            $("#kreirajDatumRojstva").val(party.dateOfBirth);
        },
        error: function(err) {
        	$("#preberiSporocilo").html("<span class='obvestilo label " +
            "label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
        }
    });
}

function preberiEHRodBolnika() {
    var stevilka = document.getElementById("preberiEHRid").value;
    izpisiPodatke(stevilka);
}

function vnesiMeritve() {
  var ehrId = $("#dodajVitalnoEHR").val();
  var masa = $("#dodajVitalnoTelesnaTeza").val();
  var visina = $("#dodajVitalnoTelesnaVisina").val();
	if (!masa || !visina || !ehrId) {
		$("#kreirajSporociloVnos").html("<span class='obvestilo label " + "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	}
	else {
	  $("#kreirajSporociloVnos").html("<span class='obvestilo label " + "label-success fade-in'>Podatki uspešno vneseni</span>");
		$.ajaxSetup({
      headers: {
        "Authorization": getAuthorization()
      }
    });
    var compositionData = {
      "ctx/language": "en",
      "ctx/territory": "SI",
      "vital_signs/height_length:0/any_event:0/body_height_length|magnitude": visina,
      "vital_signs/height_length:0/any_event:0/body_height_length|unit": "cm",
      "vital_signs/body_weight:0/any_event:0/body_weight|magnitude": masa,
      "vital_signs/body_weight:0/any_event:0/body_weight|unit": "kg"
    };
    var queryParams = {
      "ehrId": ehrId,
      templateId: 'Vital Signs',
      format: 'FLAT',
      committer: 'Ki vjm'
    };
    $.ajax({
      url: baseUrl + "/composition?" + $.param(queryParams),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(compositionData),
      success: function (res) {
        $("#header").html("Store composition");
          $("#result").html(res.meta.href);
      },
      error: function(err) {
      	console.log(err);
      }
    });
  }
  return ehrId;
}

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    izpisiPodatke(podatki[0]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		var podatki = $(this).val().split(" ");
		$("#preberiEHRid").val($(this).val());
		//najdiPoImenu(podatki[0], podatki[1]);
	});

  /**
   * Napolni testne vrednosti (EHR ID, telesna višina,
   * in telesna teža pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split(" ");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$.ajaxSetup({
      headers: {
        "Authorization": getAuthorization()
      }
    });
    $.ajax({
      url: baseUrl + "/view/" + podatki[0] + "/weight",
      type: 'GET',
      success: function (res) {
        if (res.length > 0) {
          $("#dodajVitalnoTelesnaTeza").val(res[0].weight);
        }
        else {
          $("#dodajVitalnoTelesnaTeza").val("");
        }
      },
      error: function(err) {
      	console.log(err);
      }
    });
    $.ajax({
      url: baseUrl + "/view/" + podatki[0] + "/height",
      type: 'GET',
      success: function (res) {
        if (res.length > 0) {
          $("#dodajVitalnoTelesnaVisina").val(res[0].height);
        }
        else {
          $("#dodajVitalnoTelesnaVisina").val("");
        }
      },
      error: function(err) {
      	console.log(err);
      }
    });
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});

function izracunajDolzine() {
  var masa = $("#dodajVitalnoTelesnaTeza").val();
  var visina = $("#dodajVitalnoTelesnaVisina").val();
  var kalorije = $("#dodajKalorije").val();
  var tehnika = $("#preberiTehniko").val();
  
  if (masa && visina && kalorije && tehnika) {
    $("#kreirajSporociloDolzin").html("<span class=''.</span>");
    var masaS = Math.round((masa - (visina - 170)/2)/10)*10;
    if (masaS < 60) masaS = 60;
    else if (masaS > 90) masaS = 90;
    pridobiPodatke(function(data) {
      var dolzine = Math.ceil(kalorije/data.swimming[tehnika][masaS]);
      gauge1.update(dolzine);
    });
  }
  else {
    $("#kreirajSporociloDolzin").html("<span class='obvestilo label " + "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
  }
  
}

/*function pridobiPodatke(callback) {

  "https://drive.google.com/file/d/1pdzjuhPR1bns9soUrU4mfIKOIi4PPA99/view?usp=sharing"
  $.getJSON("knjiznice/json/swimming.json", function (data) {
    callback(data.swimming)
  });
}*/

function pridobiPodatke(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://api.myjson.com/bins/rk87a", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        // vrnemo rezultat
        callback(json);
    }
  };
  xobj.send(null);
}