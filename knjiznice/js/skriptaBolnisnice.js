/* global L, distance */

var pot;

// seznam z markerji na mapi
var markerji = [];

var mapa;
var obmocje;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

var p1 = null;
var p2 = null;
var p3 = null;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Objekt oblačka markerja
  var popup = L.popup();

  izrisiBolnisnice();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    popup
      .setLatLng(latlng)
      .setContent("Izbrana točka:" + latlng.toString())
      .openOn(mapa);
    
    //prikazPoti(latlng);
    var c1 = markerji[0];
    var dc1 = (Math.pow(markerji[0]._latlngs[0][0].lat - latlng.lat, 2) + Math.pow(markerji[0]._latlngs[0][0].lng - latlng.lng, 2));
    var c2 = markerji[1];
    var dc2 = (Math.pow(markerji[1]._latlngs[0][0].lat - latlng.lat, 2) + Math.pow(markerji[1]._latlngs[0][0].lng - latlng.lng, 2));
    var c3 = markerji[2];
    var dc3 = (Math.pow(markerji[2]._latlngs[0][0].lat - latlng.lat, 2) + Math.pow(markerji[2]._latlngs[0][0].lng - latlng.lng, 2));
    
    for (var i = 3; i < markerji.length; i++) {
      if ((Math.pow(markerji[i]._latlngs[0][0].lat - latlng.lat, 2) + Math.pow(markerji[i]._latlngs[0][0].lng - latlng.lng, 2)) < dc1) {
        c3 = c2;
        dc3 = dc2;
        c2 = c1;
        dc2 = dc1;
        c1 = markerji[i];
        dc1 = (Math.pow(markerji[i]._latlngs[0][0].lat - latlng.lat, 2) + Math.pow(markerji[i]._latlngs[0][0].lng - latlng.lng, 2));
      }
      else if ((Math.pow(markerji[i]._latlngs[0][0].lat - latlng.lat, 2) + Math.pow(markerji[i]._latlngs[0][0].lng - latlng.lng, 2)) < dc2) {
        c3 = c2;
        dc3 = dc2;
        c2 = markerji[i];
        dc2 = (Math.pow(markerji[i]._latlngs[0][0].lat - latlng.lat, 2) + Math.pow(markerji[i]._latlngs[0][0].lng - latlng.lng, 2));
      }
      else if ((Math.pow(markerji[i]._latlngs[0][0].lat - latlng.lat, 2) + Math.pow(markerji[i]._latlngs[0][0].lng - latlng.lng, 2)) < dc3) {
        c3 = markerji[i];
        dc3 = (Math.pow(markerji[i]._latlngs[0][0].lat - latlng.lat, 2) + Math.pow(markerji[i]._latlngs[0][0].lng - latlng.lng, 2));
      }
    }
    if (p1 != null) {
      p1.options.color = "blue";
      p2.options.color = "blue";
      p3.options.color = "blue";
      mapa.removeLayer(p1);
      mapa.removeLayer(p2);
      mapa.removeLayer(p3);
      mapa.addLayer(p1);
      mapa.addLayer(p2);
      mapa.addLayer(p3);
    }
    
    c1.options.color = "green";
    c2.options.color = "green";
    c3.options.color = "green";
    mapa.removeLayer(c1);
    mapa.removeLayer(c2);
    mapa.removeLayer(c3);
    mapa.addLayer(c1);
    mapa.addLayer(c2);
    mapa.addLayer(c3);
    p1 = c1;
    p2 = c2;
    p3 = c3;
  }

  mapa.on('click', obKlikuNaMapo);
});


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah,
 * z dodatnim opisom, ki se prikaže v oblačku ob kliku in barvo
 * ikone, glede na tip oznake (FRI = rdeča, druge fakultete = modra in
 * restavracije = zelena)
 * 
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 * @param tip "FRI", "restaurant" ali "faculty"
 */
function dodajMarker(lat, lng, opis, tip) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-' + 
      ('red') + 
      '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});

  // Izpišemo želeno sporočilo v oblaček
  marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();

  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  markerji.push(marker);
}

function dodajMarker(lat, lng, opis, tip) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-' + 
      ('red') + 
      '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});

  // Izpišemo želeno sporočilo v oblaček
  marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();

  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  markerji.push(marker);
}


function dodajPoligon(latlng, opis, tip) {

  var poligon = L.polygon(latlng, {color: "blue", weight: 1});

  // Dodamo točko na mapo in v seznam
  poligon.addTo(mapa);
  markerji.push(poligon);
  
  // Izpišemo želeno sporočilo v oblaček
  poligon.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();
}

function pridobiPodatke(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        // vrnemo rezultat
        callback(json);
    }
  };
  xobj.send(null);
}

/**
 * Na podlagi podanih interesnih točk v GeoJSON obliki izriši
 * posamezne točke na zemljevid
 * 
 * @param jsonRezultat interesne točke v GeoJSON obliki
 */
function izrisRezultatov(jsonRezultat) {
  var znacilnosti = jsonRezultat.features;
  for (var i = 0; i < znacilnosti.length; i++) {
    var jeObmocje = 
      typeof(znacilnosti[i].geometry.coordinates[0]) == "object";
    var opis = znacilnosti[i].properties.name;
    // pridobimo koordinate
    if (typeof(znacilnosti[i].geometry.coordinates[0]) == "object") {
      if (znacilnosti[i].geometry.type == "Polygon") {
        var lanlat = [];
        var lanlatK = [];
        for (var j = 0; j < znacilnosti[i].geometry.coordinates.length; j++) {
          for (var k = 0; k < znacilnosti[i].geometry.coordinates[j].length; k++) {
            var lanlatT = [];
            lanlatT.push(znacilnosti[i].geometry.coordinates[j][k][1]);
            lanlatT.push(znacilnosti[i].geometry.coordinates[j][k][0]);
            lanlat.push(lanlatT);
          }
          lanlat.push(lanlat[0]);
          lanlatK.push(lanlat);
          lanlat = [];
        }
        dodajPoligon(lanlatK, opis, znacilnosti[i].properties.amenity);
      }
      else {
        for (var j = 0; j < znacilnosti[i].geometry.coordinates.length; j++) {
            var lanlatT = [];
            lanlatT.push(znacilnosti[i].geometry.coordinates[j][1]);
            lanlatT.push(znacilnosti[i].geometry.coordinates[j][0]);
            lanlat.push(lanlatT);
        }
        lanlat.push(lanlat[0]);
        dodajPoligon(lanlat, opis, znacilnosti[i].properties.amenity);
      }
    }
    else {
      var lng = znacilnosti[i].geometry.coordinates[0];
      var lat = znacilnosti[i].geometry.coordinates[1];
      //dodajMarker(lat, lng, opis, znacilnosti[i].properties.amenity);
      dodajPoligon([[lat, lng], [lat, lng]], opis, znacilnosti[i].properties.amenity);
    }
  }
}

function izrisiBolnisnice() {
  pridobiPodatke(function(jsonRezultat) {
    izrisRezultatov(jsonRezultat);
  });
}